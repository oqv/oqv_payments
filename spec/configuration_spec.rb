require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Configuration do
  it { is_expected.to respond_to(:auth_url) }
  it { is_expected.to respond_to(:auth_user) }
  it { is_expected.to respond_to(:auth_pass) }
  it { is_expected.to respond_to(:merchant_account) }
end

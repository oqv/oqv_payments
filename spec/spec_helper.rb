require 'rspec'
require 'oqv_payments/version'
require 'pry'
require 'vcr'
require 'byebug'

VCR.configure do |config|
  config.cassette_library_dir = "fixtures/vcr_cassettes"
  config.hook_into :webmock
end

include OqvPayments

require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::Paper do
    let! (:request) do
      described_class.new(
        {
          amount: {
            value: "100",
            currency: "BRL"
          },
          billing_address: {
            city: "São Paulo",
            country: "BR",
            house_number_or_name: "999",
            postal_code: "04787910",
            state_or_province: "SP",
            street: "Roque Petroni Jr"
          },
          shopper_name: {
            first_name: "José",
            last_name: "Silva"
          },
          delivery_date: "3",
          reference: "Teste Boleto",
          selected_brand: "boletobancario_bradesco",
          shopper_statement: "Aceitar o pagamento até 15 dias após o vencimento.&#xA;Não cobrar juros. Não aceitar o pagamento com cheque",
          social_security_number: "56861752509"
        }
      )
    end
    
  it { is_expected.to respond_to(:to_json).with(0).arguments }
  it { is_expected.to respond_to(:as_json).with(0).arguments }

  context "when valid" do
    it { is_expected.to respond_to :amount }
    it { is_expected.to respond_to :billing_address }
    it { is_expected.to respond_to :delivery_date }
    it { is_expected.to respond_to :reference }
    it { is_expected.to respond_to :merchant_account }
    it { is_expected.to respond_to :selected_brand }
    it { is_expected.to respond_to :shopper_name }
    it { is_expected.to respond_to :shopper_statement }
    it { is_expected.to respond_to :social_security_number }
  end

  context 'having a valid serialized output' do
    subject { request.as_json }

    it { is_expected.to have_key(:amount) }
    it { is_expected.to have_key(:billingAddress) }
    it { is_expected.to have_key(:shopperName) }
    it { is_expected.to have_key(:deliveryDate) }
    it { is_expected.to have_key(:reference) }
    it { is_expected.to have_key(:merchantAccount) }
    it { is_expected.to have_key(:selectedBrand) }
    it { is_expected.to have_key(:shopperStatement) }
    it { is_expected.to have_key(:socialSecurityNumber) }

    it "should respond to valid" do
      expect(request.valid?).to be_truthy
    end
  end

  context "when dispatched" do
    it "should return 200" do
      response = request.dispatch
      expect(response['status']).to be(200)
    end
  end

end

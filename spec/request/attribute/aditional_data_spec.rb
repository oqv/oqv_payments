require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::Attribute::AdditionalData do
  it { is_expected.to respond_to(:to_json).with(0).arguments }
  it { is_expected.to respond_to(:as_json).with(0).arguments }

  context 'having a valid serialized output' do
    let(:additional_data) { described_class.new card_encrypted_json: 'yay' }

    it 'must be valid if we have json' do
      expect(additional_data.valid?).to be_truthy
    end

    it 'must be invalid if we don\'t have json' do
      additional_data.card_encrypted_json = ''
      expect(additional_data.valid?).to be_falsy
    end

    subject { additional_data.as_json }

    it { is_expected.to have_key('card.encrypted.json') }
  end
end

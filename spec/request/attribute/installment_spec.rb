require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::Attribute::Installment do
  let(:installment) { described_class.new({value: 3}) }

  subject{ installment }

  it { is_expected.to respond_to :value }

  it 'should set value a valid number' do
    expect(installment.value).to be_a_kind_of(Numeric)
  end

  it 'should not aceept values equal or below zero' do
    installment.value = 0
    expect(installment.valid?).to be_falsy

    installment.value = -1
    expect(installment.valid?).to be_falsy
  end

  it "should respond to valid" do
    expect(installment.valid?).to be_truthy
  end
end

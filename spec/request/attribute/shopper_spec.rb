require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::Attribute::Shopper do
  it { is_expected.to respond_to(:to_json).with(0).arguments }
  it { is_expected.to respond_to(:as_json).with(0).arguments }

  context 'having a valid serialized output' do
    let(:cancel) { described_class.new }
    subject { cancel.as_json }

    it { is_expected.to have_key(:firstName) }
    it { is_expected.to have_key(:lastName) }
    it { is_expected.to have_key(:infix) }
    it { is_expected.to have_key(:gender) }
    it { is_expected.to have_key(:dateOfBirthDayOfMonth) }
    it { is_expected.to have_key(:dateOfBirthMonth) }
    it { is_expected.to have_key(:dateOfBirthYear) }
  end
end

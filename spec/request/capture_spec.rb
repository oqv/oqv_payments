require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::Capture do
  it { is_expected.to respond_to(:to_json).with(0).arguments }
  it { is_expected.to respond_to(:as_json).with(0).arguments }

  context 'having a valid serialized output' do
    let(:capture) { described_class.new }
    subject { capture.as_json }

    it { is_expected.to have_key(:modificationAmount) }
    it { is_expected.to have_key(:originalReference) }
    it { is_expected.to have_key(:reference) }
    it { is_expected.to have_key(:merchantAccount) }
  end
end

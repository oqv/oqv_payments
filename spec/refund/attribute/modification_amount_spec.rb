require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Refund::Attribute::ModificationAmount do
  let(:amount) { described_class.new({value: 100, currency: 'BRL'}) }

  subject{ amount }

  it { is_expected.to respond_to :value }
  it { is_expected.to respond_to :currency }

  it 'should set value to a valid string of number' do
    expect(amount.value.to_i).to be_a_kind_of(Numeric)
  end

  it 'should set currency to a valid string' do
    expect(amount.currency).to be_a_kind_of(String)

    amount.currency = ''
    expect(amount.valid?).to be_falsy
  end

  it 'should not aceept values equal or below zero' do
    amount.value = 0
    expect(amount.valid?).to be_falsy

    amount.value = -1
    expect(amount.valid?).to be_falsy
  end

  it "should respond to valid" do
    expect(amount.valid?).to be_truthy
  end
end

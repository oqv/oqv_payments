require 'active_support/core_ext/module/attribute_accessors'
require 'active_support/core_ext/array'
require 'active_support/time'
require 'business_time'

require 'oqv_payments/version'
require 'oqv_payments/configuration'
require 'oqv_payments/errors'
require 'oqv_payments/attribute_serializer'

require 'oqv_payments/request/base'
require 'oqv_payments/request/credit_card'
require 'oqv_payments/request/credit_card_list'
require 'oqv_payments/request/credit_card_disabler'
require 'oqv_payments/request/paper'
require 'oqv_payments/request/capture'
require 'oqv_payments/request/cancel'

require 'oqv_payments/request/attribute/base'
require 'oqv_payments/request/attribute/generic_address'

require 'oqv_payments/request/attribute/auth'
require 'oqv_payments/request/attribute/amount'
require 'oqv_payments/request/attribute/billing_address'
require 'oqv_payments/request/attribute/shopper_name'
require 'oqv_payments/request/attribute/card'
require 'oqv_payments/request/attribute/additional_data'
require 'oqv_payments/request/attribute/due_date'
require 'oqv_payments/request/attribute/recurring'
require 'oqv_payments/request/attribute/installment'
require 'oqv_payments/request/attribute/modification_amount'
require 'oqv_payments/request/attribute/delivery_address'
require 'oqv_payments/request/attribute/billing_address'

require 'oqv_payments/refund/base'
require 'oqv_payments/refund/request'
require 'oqv_payments/refund/attribute/base'
require 'oqv_payments/refund/attribute/modification_amount'

module OqvPayments
  mattr_reader :config

  def self.configure(&block)
    @@config = Configuration.new
    yield(@@config)

    @@config
  end

  OqvPayments.configure {}
end

module OqvPayments
  # AttributeSerializer
  # ===
  # Helper module used to serialize attributes to adyen json format
  module AttributeSerializer
    # Serialize class attributes to json
    def as_json
      self.class::ATTRIBUTES.map do |attr|
        value = instance_variable_get("@#{attr}")

        if value.nil?
          json_value = nil
        else
          if value.respond_to?(:as_json)
            json_value = value.as_json
          else
            json_value = value.to_s
          end

        end

        if attr == :shopper_ip
          { :shopperIP => json_value }
        else
          { attr.to_s.camelize(:lower).to_sym => json_value }
        end

      end.inject(&:merge)
    end

    # Converts the generated hash to a valid json string
    def to_json
      JSON.generate(as_json)
    end
  end
end

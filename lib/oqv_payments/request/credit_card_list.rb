module OqvPayments
  module Request

    class CreditCardList < Base
      include OqvPayments::AttributeSerializer

      # Attributes to be searialized to adyen
      ATTRIBUTES = [
        :merchant_account,
        :shopper_reference,
        :contract
      ].freeze

      attr_accessor *ATTRIBUTES

      def initialize(*args)
        options = args.extract_options!
        merchant =  options[:merchant_account] || OqvPayments.config.merchant_account

        @merchant_account = merchant
        @shopper_reference = options[:shopper_reference] if options[:shopper_reference].present?
        @contract = options[:contract] if options[:contract].present?
      end

      # Validates the current credit card request data
      def valid?
        unless @merchant_account.present?
          @last_error = 'Faltando configuração "merchant_account"'
          return false
        end

        unless @shopper_reference.present?
          @last_error = 'Faltando referência do comprador'
          return false
        end

        unless @contract.present?
          @last_error = 'Faltando tipo de contrato'
          return false
        end

        true
      end

      def list_cards
        body_js = {
          "merchantAccount": @merchant_account,
          "recurring": {
            "contract": @contract
          },
          "shopperReference": @shopper_reference
        }
        response = HTTP.basic_auth(auth_adyen).post(OqvPayments.config.list_cards_url, json: body_js)
        json_response = JSON.parse(response.body) rescue []
        cards = []
        if json_response["details"] && json_response["details"].length > 0
          json_response["details"].each do |ca|
            response_card = ca["RecurringDetail"]["card"]
            payment_method_variant = ca["RecurringDetail"]["paymentMethodVariant"]
            variant = ca["RecurringDetail"]["variant"]
            card = {
              holder_name: response_card["holderName"],
              expiry_month: response_card["expiryMonth"],
              expiry_year: response_card["expiryYear"],
              number: response_card["number"],
              recurring_detail_reference: ca["RecurringDetail"]["recurringDetailReference"],
              payment_method_variant: payment_method_variant,
              variant: variant,
              creation_date: ca["RecurringDetail"]["creationDate"]
            }
            card[:payment_method_variant] = CARD_VARIANTS[payment_method_variant] if CARD_VARIANTS.include?(payment_method_variant)
            card[:variant] = CARD_VARIANTS[variant] if CARD_VARIANTS.include?(variant)
            cards << card
          end
        end
        cards
      end

      def disable_cards

      end

    end
  end
end

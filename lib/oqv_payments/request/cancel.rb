module OqvPayments
  module Request
    # Cancel
    # ===
    # This class is used to make a cancel from an authorised request
    #
    # Creation
    # ---
    # ```ruby
    # request = OqvPayments::Request::Cancel.new(
    #   {
    #     original_reference: "34612836418723462", # The original pspReference from authorisation
    #     reference: "pada1" # A reference for this request(normally the order_number)
    #   }
    # )
    # ```
    class Cancel < Base
      include OqvPayments::AttributeSerializer

      # Attributes to be searialized to adyen
      ATTRIBUTES = [
        :merchant_account,
        :original_reference,
        :reference
      ]

      def initialize(*args)
        options = args.extract_options!
        merchant =  options[:merchant_account] || OqvPayments.config.merchant_account

        @reference = options[:reference]
        @original_reference = options[:original_reference]
        @merchant_account = merchant
      end

      # Validates the current cancel request data
      def valid?
        unless @merchant_account.present?
          @last_error = 'Faltando configuração "merchant_account"'
          return false
        end

        unless @original_reference
          @last_error = 'Referência original não especificada'
          return false
        end

        unless @reference
          @last_error = 'Referência não especificada'
          return false
        end

        true
      end

      def dispatch
        unless valid?
          raise OqvPayments::CancelError.new(last_error)
        end

        retries = 0

        while true
          if retries >= OqvPayments.config.cancel_retry_count
            raise OqvPayments::CancelRetryExceededError
          end

          request_params = self.as_json.reject { |k, v| v == nil }

          response = HTTP.basic_auth(auth_adyen).post(OqvPayments.config.cancel_url, json: request_params)
          json = response.parse rescue nil

          if response.status == 401 || response.status == 403
            raise OqvPayments::UnauthorizedRequestError.new
          elsif response.status == 422
            raise OqvPayments::UnprocessableEntityError.new(json['message'], json)
          elsif response.status == 200
            if json['response'] == '[cancel-received]'
              return json
            else
              raise OqvPayments::CancelError.new(json['response'], request_params)
            end
          end
        end
      end

    end
  end
end

module OqvPayments
  module Request
    module Attribute
      # BillingAddress
      # ===
      # Class used to serialize client billing address
      class BillingAddress < GenericAddress
      end
    end
  end
end

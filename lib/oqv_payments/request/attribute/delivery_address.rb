module OqvPayments
  module Request
    module Attribute
      # DeliveryAddress
      # ===
      # Class used to serialize delivery address from the order
      class DeliveryAddress < GenericAddress
      end
    end
  end
end

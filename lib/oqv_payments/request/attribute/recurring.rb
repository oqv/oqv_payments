module OqvPayments
  module Request
    module Attribute
      class Recurring < Base
        include OqvPayments::AttributeSerializer

        # Attributes to be searialized to adyen
        ATTRIBUTES = [
          :contract
        ].freeze

        attr_accessor *ATTRIBUTES

        def initialize(*args)
          options = args.extract_options!

          @contract = options[:contract]
        end

        def valid?
          return false unless @contract.present?

          true
        end
      end
    end
  end
end

module OqvPayments
  module Request
    module Attribute
      # Card
      # ===
      # This class stores the credit card fields needed to use with an avs request
      #
      # Creation
      # ---
      # ```ruby
      # card = OqvPayments::Request::Attribute::Card.new
      # card.number = '1234567890123456'
      # card.expiry_month = 12
      # card.expiry_year = 2009
      # card.cvc = 725
      # card.holder_name = 'Foo Bar'
      # ```
      class Card < Base
        include OqvPayments::AttributeSerializer

        # Attributes to be searialized to adyen
        ATTRIBUTES = [
          :number,
          :expiry_month,
          :expiry_year,
          :cvc,
          :holder_name
        ].freeze

        attr_accessor *ATTRIBUTES

        # You can pass the following attributes
        #
        # * number
        # * expiry_month
        # * expiry_year
        # * cvc
        # * holder_name
        def initialize(*args)
          options = args.extract_options!

          @number = options[:number]
          @expiry_month = options[:expiry_month]
          @expiry_year = options[:expiry_year]
          @cvc = options[:cvc]
          @holder_name = options[:holder_name]
        end

        def valid?
          # return @number.present? ? @number.to_s =~ /^[\d]{16}$/ : false
          # return @expiry_month.present? ? @expiry_month.to_i < 1 || @expiry_month.to_i > 12 : false
          # return @expiry_year.present? ? @expiry_year.to_i < DateTime.now.year : false
          # return @cvc.present? ? @cvc.to_s =~ /^[\d]{3}$/ : false
          # return @holder_name.present? ? @holder_name.to_s.strip.blank? : false
          if @number
            return false unless @number.to_s =~ /^[\d]{16}$/
          end
          if @expiry_month
            return false if @expiry_month.to_i < 1 || @expiry_month.to_i > 12
          end
          if @expiry_year
            return false if @expiry_year.to_i < DateTime.now.year
          end
          if @cvc
            return false unless @cvc.to_s =~ /^[\d]{3}$/
          end
          if @holder_name
            return false if @holder_name.to_s.strip.blank?
          end

          true
        end
      end
    end
  end
end

module OqvPayments
  module Request
    module Attribute
      class ShopperName < Base
        include OqvPayments::AttributeSerializer

        # Attributes to be searialized to adyen
        ATTRIBUTES = [
          :first_name,
          :last_name,
          :infix,
          :gender
        ].freeze

        attr_accessor *ATTRIBUTES

        def initialize(*args)
          options = args.extract_options!

          @first_name = options[:first_name]
          @last_name = options[:last_name]
          @infix = options[:infix]
          @gender = options[:gender]
        end
      end
    end
  end
end

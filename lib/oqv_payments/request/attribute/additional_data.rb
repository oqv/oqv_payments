module OqvPayments
  module Request
    module Attribute
      class AdditionalData < Base
        attr_accessor :card_encrypted_json

        def initialize(*args)
          options = args.extract_options!

          @card_encrypted_json = options[:card_encrypted_json]
        end

        def valid?
          return false unless @card_encrypted_json.present?

          true
        end

        def as_json
          { 'card.encrypted.json' => @card_encrypted_json }
        end

        def to_json
          JSON.generate(as_json)
        end
      end
    end
  end
end

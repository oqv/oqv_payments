module OqvPayments
  module Request
    class Base
      attr_reader :last_error

      def valid?
        true
      end

      def auth_adyen
        {
          user: OqvPayments.config.auth_user,
          pass: OqvPayments.config.auth_pass
        }
      end

      CARD_VARIANTS = {
        "mc" => "mastercard",
        "visa" => "visa",
        "amex" => "amex",
        "diners" => "diners"
      }

      # Send the request to adyen authrization endpoint
      def dispatch
        unless valid?
          raise OqvPayments::OqvPaymentError.new(last_error)
        end

        request_params = self.as_json.reject { |k, v| v == nil }
        response = HTTP.basic_auth(auth_adyen).post(OqvPayments.config.auth_url, json: request_params)
        json = response.parse rescue nil

        if response.status == 401 || response.status == 403
          raise OqvPayments::UnauthorizedRequestError.new((json['message'] rescue 'Erro de login'), json)
        elsif response.status == 422
          raise OqvPayments::UnprocessableEntityError.new(json['message'], json)
        elsif response.status == 200
          json.merge!({'status' => response.status.code})

          raise OqvPayments::OqvPaymentError.new(json['refusalReason'], json) if json['refusalReason'].present?

          json
        else
          raise OqvPayments::OqvPaymentError.new(json['message'], json)
        end
      end
    end
  end
end

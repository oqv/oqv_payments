module OqvPayments
  module Refund
    module Attribute
      class ModificationAmount < Base
        include OqvPayments::AttributeSerializer

        # Attributes to be searialized to adyen
        ATTRIBUTES = [
          :value,
          :currency
        ].freeze

        attr_accessor *ATTRIBUTES

        def initialize(*args)
          options = args.extract_options!

          @value = (options[:value] * 100).to_i rescue nil
          @currency = options[:currency]
        end

        def valid?
          return false if @value.to_i <= 0
          return false if @currency.to_s.blank?

          true
        end
      end
    end
  end
end
